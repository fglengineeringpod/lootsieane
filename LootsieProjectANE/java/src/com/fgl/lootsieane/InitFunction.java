package com.fgl.lootsieane;

import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.lootsie.Globals.LOOTSIE_LOGGING_LEVEL;
import com.lootsie.Lootsie;
 
public class InitFunction implements FREFunction { 
	public static final String KEY = "initFunctionKey"; 
	private String tag; 
 
	public FREObject call(FREContext arg0, FREObject[] arg1) { 
		ExtensionContext ctx = (ExtensionContext) arg0; 
		tag = ctx.getIdentifier() + "." + KEY; 
		Log.d( tag, "Invoked " + KEY ); 
		 		 
		try { 
			// Read in the public key
			FREObject input = arg1[0]; 
			String publicKey = input.getAsString();
			
			Log.d(tag, "PublicKey received, attempting to init");
			Log.d(tag, publicKey);
			
			Lootsie.setLogLevel(LOOTSIE_LOGGING_LEVEL.VERBOSE);
			
			Log.d(tag, "Lootsie: debug logging enabled");
			
			// Init Lootsie
			Lootsie.init(arg0.getActivity().getApplication(), publicKey);
			
			Lootsie.setLogLevel(LOOTSIE_LOGGING_LEVEL.VERBOSE);
			
		} catch (Exception e) { 
			Log.d(tag,"ERROR");
			Log.d(tag, e.getMessage()); 
			e.printStackTrace(); 
		} 	
		
		return null;
	} 
}