package
{
	import com.fgl.lootsieswc.LootsieExtension;
	
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class LootsieTester extends Sprite
	{
		private var exampleExtension:LootsieExtension;
		
		private var webButton:SimpleButton = new SimpleButton();
		private var achieveButton:SimpleButton = new SimpleButton();
		
		public function LootsieTester()
		{
			super();
			
			trace("LootsieTester v2");
			
			// support autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			exampleExtension = new LootsieExtension(); 
			
			var input:Boolean = true; 
			
			exampleExtension.init("F1213BFA2BC521E8B0EACF2AFA338B850D9A9D6D86625053E1230820C037F4D7");
								
			var myButtonSprite:Sprite = new Sprite();
			myButtonSprite.graphics.lineStyle(1, 0x555555);
			myButtonSprite.graphics.beginFill(0xCC0000,1);
			myButtonSprite.graphics.drawRect(0,0,200,150);
			myButtonSprite.graphics.endFill();
			
			webButton.overState = webButton.downState = webButton.upState = webButton.hitTestState = myButtonSprite;
			webButton.x = 200;
			webButton.y = 200;
			addChild(webButton);
			
			myButtonSprite = new Sprite();
			myButtonSprite.graphics.lineStyle(1, 0x555555);
			myButtonSprite.graphics.beginFill(0x00CC00,1);
			myButtonSprite.graphics.drawRect(0,0,200,150);
			myButtonSprite.graphics.endFill();
			
			achieveButton.overState = achieveButton.downState = achieveButton.upState = achieveButton.hitTestState = myButtonSprite;
			achieveButton.x = 200;
			achieveButton.y = 800;
			addChild(achieveButton);
			
			achieveButton.addEventListener(MouseEvent.CLICK,onClick);
			webButton.addEventListener(MouseEvent.CLICK,onWebClick);
		}
		
		protected function onClick(e:Event):void {
			trace("SeT achieve");
			exampleExtension.awardAchievement("timer");
			//exampleExtension.showWebView();
		}
		
		protected function onWebClick(e:Event):void {
			trace("Show Web View");
			exampleExtension.showWebView();
		}
	}
}